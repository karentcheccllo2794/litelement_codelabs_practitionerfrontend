import { LitElement, html } from 'lit-element';
import "../";
class GestorEvento extends LitElement {

    static get properties(){
        return{

        };
    }

    constructor(){
        super();
    }
    render() {
        return html `
            <h3>Gestor Evento!</h3>
            <emisor-evento @test-event="${this.processEvent}"></emisor-evento>
            <receptor-evento></receptor-evento>
        `;
    }    
   processEvent(e){
       console.log("Se ha capturado el evento del emisor");
       console.log(e.detail);
       this.shadowRoot.getElementById("receiver").course=e.detaiils
   }

}
customElements.define('gestor-evento', GestorEvento)